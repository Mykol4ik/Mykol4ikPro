﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worder : MonoBehaviour {

    public Rigidbody Sphere;
    public float Power = 10;

    // Use this for initialization
    void Start() {
        Sphere = this.gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.A))
        {
            Sphere.AddForce(Vector3.left);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Sphere.AddForce(Vector3.right);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Sphere.AddForce(Vector3.back);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            Sphere.AddForce(Vector3.forward);
        }
    }
}
